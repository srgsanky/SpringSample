package com.amazon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by sanka on 5/19/16.
 */
public class ActivityB {
    public void setPersistenceClient(PersistenceClient persistenceClient) {
        this.persistenceClient = persistenceClient;
    }

    private PersistenceClient persistenceClient;


    public void enact() {
        persistenceClient.sayHello();
    }
}
