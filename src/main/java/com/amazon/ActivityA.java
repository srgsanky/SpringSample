package com.amazon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by sanka on 5/19/16.
 */
@Component
public class ActivityA {
    @Autowired
    @Qualifier("realClient")
    PersistenceClient persistenceClient;

    public void enact() {
        persistenceClient.sayHello();
    }
}
