import com.amazon.ActivityA;
import com.amazon.ActivityB;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by sanka on 5/19/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:application.xml")
public class SampleTest {

    @Autowired
    ActivityA activityA;

    @Autowired
    ActivityB activityB;

    @Test
    public void testSomething() {
        activityA.enact();
        activityB.enact();
    }
}
